package com.mooc.bean;

public class MoocChapter {
    private Integer chapterId;

    private String chapterName;

    private String chapterIntroduce;

    private Integer courseId;

    public Integer getChapterId() {
        return chapterId;
    }

    public void setChapterId(Integer chapterId) {
        this.chapterId = chapterId;
    }

    public String getChapterName() {
        return chapterName;
    }

    public void setChapterName(String chapterName) {
        this.chapterName = chapterName == null ? null : chapterName.trim();
    }

    public String getChapterIntroduce() {
        return chapterIntroduce;
    }

    public void setChapterIntroduce(String chapterIntroduce) {
        this.chapterIntroduce = chapterIntroduce == null ? null : chapterIntroduce.trim();
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }
}