package com.mooc.bean;

import java.util.Date;

public class MoocCollect extends MoocCollectKey {
    private Date createTime;

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}