package com.mooc.bean;

public class MoocCourse {
    private Integer courseId;

    private String courseName;

    private Integer sectionId;

    private String courseIntroduce;

    private Integer courseIscombat;

    private Integer courseLevel;

    private Integer courseUsernum;

    private Integer courseIsfree;

    private Double coursePrice;

    private String courseMustKnow;

    private String courseTeacherSay;

    private Integer teacherId;

    private Integer isDeleted;

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName == null ? null : courseName.trim();
    }

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public String getCourseIntroduce() {
        return courseIntroduce;
    }

    public void setCourseIntroduce(String courseIntroduce) {
        this.courseIntroduce = courseIntroduce == null ? null : courseIntroduce.trim();
    }

    public Integer getCourseIscombat() {
        return courseIscombat;
    }

    public void setCourseIscombat(Integer courseIscombat) {
        this.courseIscombat = courseIscombat;
    }

    public Integer getCourseLevel() {
        return courseLevel;
    }

    public void setCourseLevel(Integer courseLevel) {
        this.courseLevel = courseLevel;
    }

    public Integer getCourseUsernum() {
        return courseUsernum;
    }

    public void setCourseUsernum(Integer courseUsernum) {
        this.courseUsernum = courseUsernum;
    }

    public Integer getCourseIsfree() {
        return courseIsfree;
    }

    public void setCourseIsfree(Integer courseIsfree) {
        this.courseIsfree = courseIsfree;
    }

    public Double getCoursePrice() {
        return coursePrice;
    }

    public void setCoursePrice(Double coursePrice) {
        this.coursePrice = coursePrice;
    }

    public String getCourseMustKnow() {
        return courseMustKnow;
    }

    public void setCourseMustKnow(String courseMustKnow) {
        this.courseMustKnow = courseMustKnow == null ? null : courseMustKnow.trim();
    }

    public String getCourseTeacherSay() {
        return courseTeacherSay;
    }

    public void setCourseTeacherSay(String courseTeacherSay) {
        this.courseTeacherSay = courseTeacherSay == null ? null : courseTeacherSay.trim();
    }

    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public Integer getIsDeleted() {
        return isDeleted;
    }

    public void setIsDeleted(Integer isDeleted) {
        this.isDeleted = isDeleted;
    }
}