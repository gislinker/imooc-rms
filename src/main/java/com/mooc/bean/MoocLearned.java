package com.mooc.bean;

public class MoocLearned extends MoocLearnedKey {
    private Integer learnedFlag;

    private Double learnedTime;

    public Integer getLearnedFlag() {
        return learnedFlag;
    }

    public void setLearnedFlag(Integer learnedFlag) {
        this.learnedFlag = learnedFlag;
    }

    public Double getLearnedTime() {
        return learnedTime;
    }

    public void setLearnedTime(Double learnedTime) {
        this.learnedTime = learnedTime;
    }
}