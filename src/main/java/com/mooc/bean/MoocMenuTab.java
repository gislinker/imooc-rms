package com.mooc.bean;

public class MoocMenuTab {
    private Integer tabId;

    private String tabName;

    private Integer tabCourseIdA;

    private Integer tabCourseIdB;

    private Integer tabCourseIdC;

    private Integer tabCourseIdD;

    public Integer getTabId() {
        return tabId;
    }

    public void setTabId(Integer tabId) {
        this.tabId = tabId;
    }

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName == null ? null : tabName.trim();
    }

    public Integer getTabCourseIdA() {
        return tabCourseIdA;
    }

    public void setTabCourseIdA(Integer tabCourseIdA) {
        this.tabCourseIdA = tabCourseIdA;
    }

    public Integer getTabCourseIdB() {
        return tabCourseIdB;
    }

    public void setTabCourseIdB(Integer tabCourseIdB) {
        this.tabCourseIdB = tabCourseIdB;
    }

    public Integer getTabCourseIdC() {
        return tabCourseIdC;
    }

    public void setTabCourseIdC(Integer tabCourseIdC) {
        this.tabCourseIdC = tabCourseIdC;
    }

    public Integer getTabCourseIdD() {
        return tabCourseIdD;
    }

    public void setTabCourseIdD(Integer tabCourseIdD) {
        this.tabCourseIdD = tabCourseIdD;
    }
}