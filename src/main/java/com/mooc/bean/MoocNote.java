package com.mooc.bean;

import java.util.Date;

public class MoocNote extends MoocNoteKey {
    private Integer sectionId;

    private String noteContent;

    private Integer noteLike;

    private Date createTime;

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public String getNoteContent() {
        return noteContent;
    }

    public void setNoteContent(String noteContent) {
        this.noteContent = noteContent == null ? null : noteContent.trim();
    }

    public Integer getNoteLike() {
        return noteLike;
    }

    public void setNoteLike(Integer noteLike) {
        this.noteLike = noteLike;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}