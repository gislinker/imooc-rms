package com.mooc.bean;

public class MoocSection {
    private Integer sectionId;

    private String sectionName;

    private Integer chapterId;

    private Integer courseId;

    private String sectionUrl;

    private String sectionDuration;

    public Integer getSectionId() {
        return sectionId;
    }

    public void setSectionId(Integer sectionId) {
        this.sectionId = sectionId;
    }

    public String getSectionName() {
        return sectionName;
    }

    public void setSectionName(String sectionName) {
        this.sectionName = sectionName == null ? null : sectionName.trim();
    }

    public Integer getChapterId() {
        return chapterId;
    }

    public void setChapterId(Integer chapterId) {
        this.chapterId = chapterId;
    }

    public Integer getCourseId() {
        return courseId;
    }

    public void setCourseId(Integer courseId) {
        this.courseId = courseId;
    }

    public String getSectionUrl() {
        return sectionUrl;
    }

    public void setSectionUrl(String sectionUrl) {
        this.sectionUrl = sectionUrl == null ? null : sectionUrl.trim();
    }

    public String getSectionDuration() {
        return sectionDuration;
    }

    public void setSectionDuration(String sectionDuration) {
        this.sectionDuration = sectionDuration == null ? null : sectionDuration.trim();
    }
}