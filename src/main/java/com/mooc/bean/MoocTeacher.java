package com.mooc.bean;

public class MoocTeacher {
    private Integer teacherId;

    private String teacherName;

    private String teacherIntroduce;

    private String teacherTab;

    private String teacherImg;

    public Integer getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(Integer teacherId) {
        this.teacherId = teacherId;
    }

    public String getTeacherName() {
        return teacherName;
    }

    public void setTeacherName(String teacherName) {
        this.teacherName = teacherName == null ? null : teacherName.trim();
    }

    public String getTeacherIntroduce() {
        return teacherIntroduce;
    }

    public void setTeacherIntroduce(String teacherIntroduce) {
        this.teacherIntroduce = teacherIntroduce == null ? null : teacherIntroduce.trim();
    }

    public String getTeacherTab() {
        return teacherTab;
    }

    public void setTeacherTab(String teacherTab) {
        this.teacherTab = teacherTab == null ? null : teacherTab.trim();
    }

    public String getTeacherImg() {
        return teacherImg;
    }

    public void setTeacherImg(String teacherImg) {
        this.teacherImg = teacherImg == null ? null : teacherImg.trim();
    }
}