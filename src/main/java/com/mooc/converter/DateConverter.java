package com.mooc.converter;

import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


import org.springframework.core.convert.converter.Converter;

/**
 * String 转为 日期类型
 * 
 * @author jasper
 */
public class DateConverter implements Converter<String, Date> {
	
	private static final String DATE = "YYYY-MM-DD";
	private static final String DATETIME = "YYYY-MM-DD HH:MM:SS";
	private static final String TIMESTAMP = "YYYY-MM-DD HH:MM:SS.SSS";

	public Date convert(String source) {
		// source.trim() 防止输入的字符串前后有空格 
		Date date = toDate(source.trim());
		return date;
	}

	/**
	 * 日期转换 方法
	 * @param source
	 * @return
	 */
	private static Date toDate(String source) {
		Date date = new Date();
		DateFormat dateFormat = null;
		
		if (null != source && !"".equals(source)) {
			// 获取日期字符串的长度 
			int sourceLen = source.length();
			
			// 根据长度判断 source 属于那种日期类型 
			try {
				if (sourceLen <= 10) {
					dateFormat = new SimpleDateFormat(DATE, new DateFormatSymbols(Locale.CHINA));
					date = dateFormat.parse(source);
				} else if (sourceLen <= 19) {
					dateFormat = new SimpleDateFormat(DATETIME, new DateFormatSymbols(Locale.CHINA));
					date = dateFormat.parse(source);
				} else if(sourceLen <= 23) {
					dateFormat = new SimpleDateFormat(TIMESTAMP, new DateFormatSymbols(Locale.CHINA));
					date = dateFormat.parse(source);
				} else {
					System.out.println("输入日期格式不合法：" + source);
				}
			} catch (ParseException e) {
				// 打印异常信息
				System.out.println("日期转换异常：" + e.getMessage());
			}
		}
		
		return date;
	}
	
}
