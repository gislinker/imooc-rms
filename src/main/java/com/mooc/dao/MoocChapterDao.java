package com.mooc.dao;

import com.mooc.bean.MoocChapter;

public interface MoocChapterDao {
    int deleteByPrimaryKey(Integer chapterId);

    int insert(MoocChapter record);

    int insertSelective(MoocChapter record);

    MoocChapter selectByPrimaryKey(Integer chapterId);

    int updateByPrimaryKeySelective(MoocChapter record);

    int updateByPrimaryKey(MoocChapter record);
}