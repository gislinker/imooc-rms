package com.mooc.dao;

import com.mooc.bean.MoocClass;

public interface MoocClassDao {
    int deleteByPrimaryKey(Integer classId);

    int insert(MoocClass record);

    int insertSelective(MoocClass record);

    MoocClass selectByPrimaryKey(Integer classId);

    int updateByPrimaryKeySelective(MoocClass record);

    int updateByPrimaryKey(MoocClass record);
}