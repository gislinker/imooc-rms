package com.mooc.dao;

import com.mooc.bean.MoocCollect;
import com.mooc.bean.MoocCollectKey;

public interface MoocCollectDao {
    int deleteByPrimaryKey(MoocCollectKey key);

    int insert(MoocCollect record);

    int insertSelective(MoocCollect record);

    MoocCollect selectByPrimaryKey(MoocCollectKey key);

    int updateByPrimaryKeySelective(MoocCollect record);

    int updateByPrimaryKey(MoocCollect record);
}