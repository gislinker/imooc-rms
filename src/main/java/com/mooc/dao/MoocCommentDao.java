package com.mooc.dao;

import com.mooc.bean.MoocComment;

public interface MoocCommentDao {
    int deleteByPrimaryKey(Integer commentId);

    int insert(MoocComment record);

    int insertSelective(MoocComment record);

    MoocComment selectByPrimaryKey(Integer commentId);

    int updateByPrimaryKeySelective(MoocComment record);

    int updateByPrimaryKey(MoocComment record);
}