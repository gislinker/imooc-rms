package com.mooc.dao;

import com.mooc.bean.MoocCourse;

public interface MoocCourseDao {
    int deleteByPrimaryKey(Integer courseId);

    int insert(MoocCourse record);

    int insertSelective(MoocCourse record);

    MoocCourse selectByPrimaryKey(Integer courseId);

    int updateByPrimaryKeySelective(MoocCourse record);

    int updateByPrimaryKey(MoocCourse record);
}