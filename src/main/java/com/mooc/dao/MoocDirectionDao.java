package com.mooc.dao;

import com.mooc.bean.MoocDirection;

public interface MoocDirectionDao {
    int deleteByPrimaryKey(Integer directionId);

    int insert(MoocDirection record);

    int insertSelective(MoocDirection record);

    MoocDirection selectByPrimaryKey(Integer directionId);

    int updateByPrimaryKeySelective(MoocDirection record);

    int updateByPrimaryKey(MoocDirection record);
}