package com.mooc.dao;

import com.mooc.bean.MoocLearned;
import com.mooc.bean.MoocLearnedKey;

public interface MoocLearnedDao {
    int deleteByPrimaryKey(MoocLearnedKey key);

    int insert(MoocLearned record);

    int insertSelective(MoocLearned record);

    MoocLearned selectByPrimaryKey(MoocLearnedKey key);

    int updateByPrimaryKeySelective(MoocLearned record);

    int updateByPrimaryKey(MoocLearned record);
}