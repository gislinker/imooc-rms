package com.mooc.dao;

import com.mooc.bean.MoocMenuTab;

public interface MoocMenuTabDao {
    int deleteByPrimaryKey(Integer tabId);

    int insert(MoocMenuTab record);

    int insertSelective(MoocMenuTab record);

    MoocMenuTab selectByPrimaryKey(Integer tabId);

    int updateByPrimaryKeySelective(MoocMenuTab record);

    int updateByPrimaryKey(MoocMenuTab record);
}