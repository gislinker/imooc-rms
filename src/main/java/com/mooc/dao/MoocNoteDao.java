package com.mooc.dao;

import com.mooc.bean.MoocNote;
import com.mooc.bean.MoocNoteKey;

public interface MoocNoteDao {
    int deleteByPrimaryKey(MoocNoteKey key);

    int insert(MoocNote record);

    int insertSelective(MoocNote record);

    MoocNote selectByPrimaryKey(MoocNoteKey key);

    int updateByPrimaryKeySelective(MoocNote record);

    int updateByPrimaryKey(MoocNote record);
}