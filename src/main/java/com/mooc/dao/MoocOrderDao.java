package com.mooc.dao;

import com.mooc.bean.MoocOrder;

public interface MoocOrderDao {
    int deleteByPrimaryKey(String orderId);

    int insert(MoocOrder record);

    int insertSelective(MoocOrder record);

    MoocOrder selectByPrimaryKey(String orderId);

    int updateByPrimaryKeySelective(MoocOrder record);

    int updateByPrimaryKey(MoocOrder record);
}