package com.mooc.dao;

import com.mooc.bean.MoocOrderDetailKey;

public interface MoocOrderDetailDao {
    int deleteByPrimaryKey(MoocOrderDetailKey key);

    int insert(MoocOrderDetailKey record);

    int insertSelective(MoocOrderDetailKey record);
}