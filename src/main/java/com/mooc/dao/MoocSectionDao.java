package com.mooc.dao;

import com.mooc.bean.MoocSection;

public interface MoocSectionDao {
    int deleteByPrimaryKey(Integer sectionId);

    int insert(MoocSection record);

    int insertSelective(MoocSection record);

    MoocSection selectByPrimaryKey(Integer sectionId);

    int updateByPrimaryKeySelective(MoocSection record);

    int updateByPrimaryKey(MoocSection record);
}