package com.mooc.dao;

import com.mooc.bean.MoocShoppingCarKey;

public interface MoocShoppingCarDao {
    int deleteByPrimaryKey(MoocShoppingCarKey key);

    int insert(MoocShoppingCarKey record);

    int insertSelective(MoocShoppingCarKey record);
}