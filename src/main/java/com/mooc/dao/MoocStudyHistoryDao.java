package com.mooc.dao;

import com.mooc.bean.MoocStudyHistory;
import com.mooc.bean.MoocStudyHistoryKey;

public interface MoocStudyHistoryDao {
    int deleteByPrimaryKey(MoocStudyHistoryKey key);

    int insert(MoocStudyHistory record);

    int insertSelective(MoocStudyHistory record);

    MoocStudyHistory selectByPrimaryKey(MoocStudyHistoryKey key);

    int updateByPrimaryKeySelective(MoocStudyHistory record);

    int updateByPrimaryKey(MoocStudyHistory record);
}