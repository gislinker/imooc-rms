package com.mooc.dao;

import com.mooc.bean.MoocTeacher;

public interface MoocTeacherDao {
    int deleteByPrimaryKey(Integer teacherId);

    int insert(MoocTeacher record);

    int insertSelective(MoocTeacher record);

    MoocTeacher selectByPrimaryKey(Integer teacherId);

    int updateByPrimaryKeySelective(MoocTeacher record);

    int updateByPrimaryKey(MoocTeacher record);
}