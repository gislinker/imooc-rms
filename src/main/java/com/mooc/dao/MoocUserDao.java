package com.mooc.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.mooc.bean.MoocUser;

public interface MoocUserDao {
    int deleteByPrimaryKey(Integer userId);

    int insert(MoocUser record);

    int insertSelective(MoocUser record);

    MoocUser selectByPrimaryKey(Integer userId);

    int updateByPrimaryKeySelective(MoocUser record);

    int updateByPrimaryKey(MoocUser record);
    
    
    /**
     * 通过 bean 中有值的属性进行查找
     * @param moocUser
     * @return
     */
    List<MoocUser> selectBySelective(MoocUser moocUser);
    
    /**
     * 根据用户手机号查询用户
     * @param userPhone
     * @return
     */
    MoocUser selectByPhone(@Param("userPhone") String userPhone);
}