package com.mooc.service;

import com.mooc.bean.MoocUser;

public interface IUserService {

	/**
	 * 登录判断
	 * @param moocUser
	 * @return
	 */
	boolean loginJudge(MoocUser moocUser);
	
	
	/**
	 * 判断用户手机号码的唯一性
	 * @param userPhone
	 * @return
	 */
	boolean judgePhoneUnique(String userPhone);
}
