package com.mooc.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.mooc.bean.MoocUser;
import com.mooc.dao.MoocUserDao;
import com.mooc.service.IUserService;


@Service
public class UserServiceImpl implements IUserService {
	
	@Resource
	private MoocUserDao userDao;

	/**
	 * 根据登录条件查询 
	 */
	@Override
	public boolean loginJudge(MoocUser moocUser) {
		// 根据 moocUser 中含有的字段作为条件查询
		List<MoocUser> moocUserList = userDao.selectBySelective(moocUser);
		
		for (MoocUser item : moocUserList) {
			System.out.println(item);
		}
		
		if (moocUserList.size() == 0) return false;
		return true;
	}

	
	/**
	 * 判断用户手机号码的唯一性
	 */
	@Override
	public boolean judgePhoneUnique(String userPhone) {
		
		MoocUser moocUser = userDao.selectByPhone(userPhone);
		
		if (null == moocUser) return true;
		return false;
	}

}
