package com.mooc.util;

public class MyUtil {

	/**
	 * 获取 6 位验证码
	 * @return
	 */
	public static String getSecurityCode() {
		String securityCode = null;
		
		securityCode = String.valueOf((int)(Math.random()*1000000L));
		
		return securityCode;
	}
	
	
	/**
	 * 获取 16 位订单 id
	 * @return
	 */
	public static String getOrderId() {
		StringBuilder orderId = new StringBuilder();
		orderId.append(String.valueOf((int)(Math.random()*1000)));
		orderId.append(System.currentTimeMillis());
		return orderId.toString();
	}
}
